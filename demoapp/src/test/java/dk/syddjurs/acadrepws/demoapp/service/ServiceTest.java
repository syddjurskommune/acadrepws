package dk.syddjurs.acadrepws.demoapp.service;

import com.lowagie.text.DocumentException;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.UUID;

@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class ServiceTest {
    @Autowired
    AcadreService acadreService;

    @Autowired
    PdfService pdfService;

    @Test
    public void printMemoSheetHtmlTest() {
        var memoHtml = acadreService.getMemoSheetHtml(757413);
        log.info(memoHtml);
    }

    @Test
    public void printMemoSheetPdfTest() throws IOException, DocumentException {
        var memoHtml = acadreService.getMemoSheetHtml(757413);
        var memoPdf = pdfService.convertHtmlToPdf(memoHtml);
        var tempDir = System.getProperty("java.io.tmpdir");
        var file = Files.write(Paths.get(tempDir,UUID.randomUUID().toString() + ".pdf"), memoPdf);
        log.info("Memo written to : " + file.toAbsolutePath().toString());
    }

}
