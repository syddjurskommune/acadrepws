package dk.syddjurs.acadrepws.demoapp.config.properties;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Acadre {
    private String endPointV4;
    private String domain;
    private String username;
    private String password;
}