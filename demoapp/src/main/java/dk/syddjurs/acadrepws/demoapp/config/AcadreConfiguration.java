package dk.syddjurs.acadrepws.demoapp.config;

import com.traen._2007._09._06.CaseService4;
import com.traen._2007._09._06.DocumentService4;
import com.traen._2007._09._06.MemoService4;
import com.traen.services.acadreservice.AcadreServiceV4;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.xml.ws.BindingProvider;

@Configuration
public class AcadreConfiguration {

    @Autowired
    private Settings settings;

//    @Bean
//    public CaseService4 caseService4() {
//        var caseService4 = AcadreServiceV4.create(AcadreServiceV4.CaseService4).getPort(CaseService4.class);
//        configurePWS((BindingProvider) caseService4, settings.getAcadre().getEndPointV4());
//        return caseService4;
//    }
//
//    @Bean
//    public DocumentService4 documentService4() {
//        var documentService4 = AcadreServiceV4.create(AcadreServiceV4.DocumentService4).getPort(DocumentService4.class);
//        configurePWS((BindingProvider) documentService4, settings.getAcadre().getEndPointV4());
//        return documentService4;
//    }

    @Bean
    public MemoService4 memoService4() {
        var memoService4 = AcadreServiceV4.create(AcadreServiceV4.MemoService4).getPort(MemoService4.class);
        configurePWS((BindingProvider) memoService4, settings.getAcadre().getEndPointV4());
        return memoService4;
    }

    private void configurePWS(BindingProvider bindingProvider, String endpointUrl) {
        // Configure endpoint
        bindingProvider.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, endpointUrl);
        // Configure authentication
        bindingProvider.getRequestContext().put(BindingProvider.USERNAME_PROPERTY, settings.getAcadre().getDomain() + "\\" + settings.getAcadre().getUsername());
        bindingProvider.getRequestContext().put(BindingProvider.PASSWORD_PROPERTY, settings.getAcadre().getPassword());
    }

}