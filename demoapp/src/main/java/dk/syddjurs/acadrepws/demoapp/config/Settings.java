package dk.syddjurs.acadrepws.demoapp.config;

import dk.syddjurs.acadrepws.demoapp.config.properties.Acadre;
import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@Getter
@Setter
@ConfigurationProperties(prefix = "settings")
public class Settings {
    private Acadre acadre;
}
