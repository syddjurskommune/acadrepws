package dk.syddjurs.acadrepws.demoapp.service;

import com.traen._2007._09._06.MemoService4;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AcadreService {
    @Autowired
    MemoService4 memoService4;

    public String getMemoSheetHtml(int caseId ) {

        return new String(memoService4.getMemoSheet(caseId));
    }
}