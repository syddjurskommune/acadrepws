package dk.syddjurs.acadrepws.demoapp.service;

import com.lowagie.text.DocumentException;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;
import org.springframework.stereotype.Service;
import org.xhtmlrenderer.pdf.ITextRenderer;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

@Service
public class PdfService {

    public byte[] convertHtmlToPdf(String html) throws DocumentException, IOException {

        // just an example of generating pdf-convertable html using Jsoup
        // it might need some tweaks
        var cleanedHtml = Jsoup.clean(html, Whitelist.relaxed().addTags("body"));
        // terminate br tags for compatibility
        cleanedHtml = cleanedHtml.replaceAll("<br>", "<br />");

        var outputStream = new ByteArrayOutputStream();
        var renderer = new ITextRenderer();
        renderer.setDocumentFromString(cleanedHtml);
        renderer.layout();
        renderer.createPDF(outputStream);
        var result = outputStream.toByteArray();
        outputStream.close();
        return result;
    }

}
